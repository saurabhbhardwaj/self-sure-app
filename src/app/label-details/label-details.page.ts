import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-tab3',
  templateUrl: 'label-details.page.html',
  styleUrls: ['label-details.page.scss']
})
export class labelDetailsPage {
  data: any;
  constructor(private route: ActivatedRoute) {

   this.route.params.subscribe(params => {
      this.data = params['data'];
     console.log("parameter is ---", this.data);
     /*if(this.data.barcode) {
       console.log("got barcode ---", this.data.barcode)
     } else {
       console.log("got tracking id ---", this.data.trackingId)
     }*/
    });
  }

}
