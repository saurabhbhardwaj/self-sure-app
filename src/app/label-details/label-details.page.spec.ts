import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { labelDetailsPage } from './label-details.page.ts';

describe('Tab3Page', () => {
  let component: labelDetailsPage;
  let fixture: ComponentFixture<labelDetailsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [labelDetailsPage],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(labelDetailsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
