import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-tab2',
  templateUrl: 'generate-return-label.page.html',
  styleUrls: ['generate-return-label.page.scss']
})
export class generateReturnLabelPage {
  currentImage: any;
  showReturnLabel :boolean;
  returnReasonChecked: boolean;
  barcode: any;
  data: any;
  constructor(private route: ActivatedRoute) {
    this.showReturnLabel = false;
    this.returnReasonChecked = true;

      this.route.params.subscribe(params => {
        this.data = params['data'];
        console.log("parameter is ---", this.data);

      });

  }

  returnReasonSelected(event) {
    console.log('selected---', event);
    this.returnReasonChecked = false;
  }
  createReturnLabel() {
    this.showReturnLabel = true;

  }

  sendBarcodeData() {
    console.log("here in sendBarcodeData--- data is ready to send", this.barcode)
  }
}
