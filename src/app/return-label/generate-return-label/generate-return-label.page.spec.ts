import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { generateReturnLabelPage } from './generate-return-label.page.ts';

describe('returnLabelPage', () => {
  let component: generateReturnLabelPage;
  let fixture: ComponentFixture<generateReturnLabelPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [generateReturnLabelPage],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(generateReturnLabelPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
