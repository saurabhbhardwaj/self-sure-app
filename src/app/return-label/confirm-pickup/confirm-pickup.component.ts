import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-confirm-pickup',
  templateUrl: './confirm-pickup.component.html',
  styleUrls: ['./confirm-pickup.component.scss'],
})
export class ConfirmPickupComponent implements OnInit {
  data
  constructor(private route: ActivatedRoute) {
    this.route.params.subscribe(params => {
      this.data = params['data'];
      console.log("parameter is ---", this.data);

    });
  }

  ngOnInit() {}

}
