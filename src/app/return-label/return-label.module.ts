import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { returnLabelPage } from './return-label.page';
import { generateReturnLabelPage } from './generate-return-label/generate-return-label.page';
import { SchedulePickupComponent} from './schedule-pickup/schedule-pickup.component';
import {ConfirmPickupComponent} from './confirm-pickup/confirm-pickup.component';
@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([{ path: '', component: returnLabelPage },
      { path: 'generateLabel', component: generateReturnLabelPage },
      { path: 'generateLabel/schedulePickup', component: SchedulePickupComponent },
      { path: 'generateLabel/schedulePickup/confirmPickup', component: ConfirmPickupComponent }])
  ],
  declarations: [returnLabelPage,
    generateReturnLabelPage,
    SchedulePickupComponent,
    ConfirmPickupComponent
  ],
  exports: [RouterModule]
})
export class ReturnLabelPageModule {}
