import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { returnLabelPage } from './return-label.page.ts';

describe('returnLabelPage', () => {
  let component: returnLabelPage;
  let fixture: ComponentFixture<returnLabelPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [returnLabelPage],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(returnLabelPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
