import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-schedule-pickup',
  templateUrl: './schedule-pickup.component.html',
  styleUrls: ['./schedule-pickup.component.scss'],
})
export class SchedulePickupComponent implements OnInit {
data;
  scheduledReturn;
  pickUpService;
  constructor( private route: ActivatedRoute) {
this.scheduledReturn = true;
    this.pickUpService = false;
    this.route.params.subscribe(params => {
      this.data = params['data'];
      console.log("parameter is ---", this.data);

    });
  }

  ngOnInit() {}
  schedulePickup(event) {
    console.log("schedulePickup----", event.detail.value)
    if(event.detail.value == 'pickup') {
      this.scheduledReturn = false;

      this.pickUpService = true;
    } else {
    this.scheduledReturn = false;
      this.pickUpService = false;
    }
  }
  confirmPickup() {

  }
}
