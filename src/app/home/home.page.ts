import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
@Component({
  selector: 'app-tab1',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage {
  currentImage: any;
  sendAsReturn :boolean;
  barcode: any;
  data :any;

  slideOpts = {
    initialSlide: 0,
    speed: 100
  };

  constructor(private barcodeScanner: BarcodeScanner, private router: Router) {
    this.sendAsReturn = true;
    this.data ={
      trackingId : ''
    }
  }
  scanBarcode() {
    this.sendAsReturn = true;

    this.barcodeScanner.scan().then(barcodeData => {
      console.log('Barcode data', barcodeData);
      this.barcode = barcodeData;
      console.log("scanning is done successfully ---", barcodeData)
      this.sendAsReturn = false;
    }).catch(err => {
      console.log('Error', err);
    });

  }

  sendBarcodeData() {
    console.log("here in sendBarcodeData--- data is ready to send with barcode", this.barcode);
    console.log("here in sendBarcodeData--- data is ready to send with input", this.data.trackingId);
    if(this.barcode) {
      this.router.navigate(['/details',  this.barcode.text]);
    }
     else {
      this.router.navigate(['/details',  this.data.trackingId]);
    }

  }
  setTrackingId(value) {
    this.sendAsReturn = true;
    this.data.trackingId = value;
   // this.readData(this.data);
  }
}
