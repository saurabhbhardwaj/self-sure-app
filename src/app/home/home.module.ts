import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

import { HomePage } from './home.page';
import { TrackReturnsPage } from './track-returns/track-returns.component';
import { TrackItemPage } from './track-item/track-item.component';
@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([{ path: '', component: HomePage },
      { path: 'track', component: TrackReturnsPage },
      { path: 'item', component: TrackItemPage }])
  ],
  providers:[BarcodeScanner],
  declarations: [HomePage, TrackReturnsPage, TrackItemPage]
})
export class HomePageModule {}
