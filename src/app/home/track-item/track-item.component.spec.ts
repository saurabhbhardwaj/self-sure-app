import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrackItemPage } from './track-item.component';

describe('TrackItemComponent', () => {
  let component: TrackItemPage;
  let fixture: ComponentFixture<TrackItemPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrackItemPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrackItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
