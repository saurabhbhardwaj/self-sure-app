import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrackReturnsPage } from './track-returns.component.ts';

describe('TrackReturnsComponent', () => {
  let component: TrackReturnsPage;
  let fixture: ComponentFixture<TrackReturnsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrackReturnsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrackReturnsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
