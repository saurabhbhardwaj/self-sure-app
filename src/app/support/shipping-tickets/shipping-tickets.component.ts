import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-shipping-tickets',
  templateUrl: './shipping-tickets.component.html',
  styleUrls: ['./shipping-tickets.component.scss'],
})
export class ShippingTicketsComponent implements OnInit {
  ticketTypes = [
    {
      "title": "Parcel Damaged",
      "subCategories": [
        "ABC", "DEC"
      ]
    },
    {
      "title": "Delivery Concerns",
      "subCategories": [
        "Late Delivery", "Unable to reach delivery person"
      ]
    }
  ];

  shownGroup = null;


  data;
  constructor(private route: ActivatedRoute,private router: Router) {
    this.route.params.subscribe(params => {
      this.data = params['data'];
      console.log("parameter is ---", this.data);

    });
  }
  ngOnInit() { }

  toggleGroup(group) {
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  }

  isGroupShown(group) {
    return this.shownGroup === group;
  }

  openTicketScreen(data) {
    let navigationExtras: NavigationExtras = {
      state: {
        data
      }
    };
    this.router.navigate(['/support/ticket'], navigationExtras);
  }
}
