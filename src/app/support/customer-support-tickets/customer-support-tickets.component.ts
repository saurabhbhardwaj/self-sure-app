// import { Component, OnInit } from '@angular/core';
//

// export class CustomerSupportTicketsComponent implements OnInit {
//
//   constructor() { }
//
//   ngOnInit() {}
//
// }
import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-customer-support-tickets',
  templateUrl: './customer-support-tickets.component.html',
  styleUrls: ['./customer-support-tickets.component.scss'],
})
export class CustomerSupportTicketsComponent implements OnInit {
  ticketTypes = [
    {
      "title": "Product Quality",
      "subCategories": [
        "Product not in working state", "Not happy with product Quality"
      ]
    },
    {
      "title": "Different model was sent",
      "subCategories": [
        "Lower model was sent", "Product doesn't match with description"
      ]
    }
  ];

  shownGroup = null;

  constructor(private router: Router) { }

  ngOnInit() { }

  toggleGroup(group) {
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  }

  isGroupShown(group) {
    return this.shownGroup === group;
  }

  openTicketScreen(data) {
    let navigationExtras: NavigationExtras = {
      state: {
        data
      }
    };
    this.router.navigate(['/support/ticket'], navigationExtras);
  }
}
