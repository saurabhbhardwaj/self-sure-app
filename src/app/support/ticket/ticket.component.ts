import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { UserData } from '../../providers/user-data';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.scss'],
})
export class TicketComponent implements OnInit {
  apiKey = 'Mm9sSXZ4YU10WmpZQU5nY0U5bjI6'
  data: any;
  username: string;
  ticketRequest = {
    description: '',
    subject: '',
    email: '',
    priority: 2,
    status: 2,
  };
  constructor(private route: ActivatedRoute, private router: Router, private httpClient: HttpClient, private userData: UserData) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.data = this.router.getCurrentNavigation().extras.state.data;
      }
    });
    this.userData.getUsername().then((username) => {
      this.username = username;
    });
  }

  ngOnInit() { }

  submitTicket() {
    this.ticketRequest.subject=this.data;
    this.ticketRequest.email=this.username;
    console.log('Submit ticket clicked', this.ticketRequest);
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Basic ' + this.apiKey
      })
    };
    this.httpClient.post(`https://bhardwajsaurabh.freshdesk.com/api/v2/tickets`,this.ticketRequest, options).subscribe((data)=>{
      console.log('Ticke posted successfully');
      this.router.navigateByUrl('/support/acknowledge');
    },
     error=>{
       console.log('Unable to submit your ticket.')
     });
  }

  getUsername() {

  }
}
