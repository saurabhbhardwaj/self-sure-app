import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import { OptionsComponent } from './options/options.component';
import { ShippingTicketsComponent } from './shipping-tickets/shipping-tickets.component';
import { CustomerSupportTicketsComponent } from './customer-support-tickets/customer-support-tickets.component';
import { TicketComponent } from './ticket/ticket.component';
import { AcknowledgeComponent } from './acknowledge/acknowledge.component';

@NgModule({
  declarations: [OptionsComponent, ShippingTicketsComponent, CustomerSupportTicketsComponent, TicketComponent, AcknowledgeComponent],
  // exports: [],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HttpClientModule,
    RouterModule.forChild([
      {
        path: 'shipping',
        component: ShippingTicketsComponent
      },
      {
        path: 'customer-support',
        component: CustomerSupportTicketsComponent
      },
      {
        path: 'ticket',
        component: TicketComponent
      },
      {
        path: 'acknowledge',
        component: AcknowledgeComponent
      }
    ]),
  ]
})
export class SupportModule { }
